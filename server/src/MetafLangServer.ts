import {
	createConnection,
	TextDocuments,
	Diagnostic,
	DiagnosticSeverity,
	ProposedFeatures,
	InitializeParams,
	DidChangeConfigurationNotification,
	CompletionItem,
	CompletionItemKind,
	TextDocumentPositionParams,
	TextDocumentSyncKind,
	InitializeResult,
	Position,
	Hover,
	MarkedString,
	CodeAction,
	CodeActionKind,
  Connection,
  ClientCapabilities,
  DidChangeConfigurationParams,
  ServerCapabilities,
  HoverParams,
  CompletionOptions,
  CompletionList,
  CompletionParams,
  SignatureHelpOptions,
  SignatureHelpParams,
  DefinitionParams,
  Definition,
  Location,
  LocationLink,
  ReferenceParams,
  DocumentHighlightParams,
  DocumentHighlight,
  RenameOptions,
  PrepareRenameParams,
  RenameParams,
  WorkspaceEdit,
  Range,
  PrepareRenameResult,
  BrowserMessageReader,
  BrowserMessageWriter
} from 'vscode-languageserver/browser';

import {
	TextDocument
} from 'vscode-languageserver-textdocument';
import MetafSettings from './MetafSettings';
import MetafValidator from './MetafValidator';
import MetafHoverProvider from './MetafHoverProvider';
import MetafCompletionProvider from './MetafCompletionProvider';
import { ModelParser } from 'metaf-parser-ts';
import MetafSignatureProvider from './MetafSignatureProvider';
import MetafDefinitionProvider from './MetafDefinitionProvider';
import MetafReferenceProvider from './MetafReferenceProvider';
import MetafHighlightProvider from './MetafHighlightProvider';
import MetafRenameProvider from './MetafRenameProvider';

export default class MetafLangServer {
  defaultSettings: MetafSettings;
  globalSettings: any;
  documentSettings: Map<string, Thenable<MetafSettings>>;
  modelParsers: Map<string, ModelParser>;
  serverCapabilities: ServerCapabilities<any>;

  public Connection: Connection;
  public Documents: TextDocuments<TextDocument>;

  public ClientCapabilities: ClientCapabilities | undefined;
  Documentation: any;
  MetafSignatureProvider: MetafSignatureProvider;
  MetafDefinitionProvider: MetafDefinitionProvider;
  MetafReferenceProvider: MetafReferenceProvider;
  MetafHighlightProvider: MetafHighlightProvider;
  MetafRenameProvider: MetafRenameProvider;
  
  public get hasConfigurationCapability() { return !!(this.ClientCapabilities?.workspace?.configuration); }
  public get hasWorkspaceFolderCapability() { return !!(this.ClientCapabilities?.workspace?.workspaceFolders); }
  public get hasDiagnosticRelatedInformationCapability() { return !!(this.ClientCapabilities?.textDocument?.publishDiagnostics?.relatedInformation); }

  public MetafValidator: MetafValidator;
  public MetafHoverProvider: MetafHoverProvider;
  public MetafCompletionProvider: MetafCompletionProvider;

  constructor() {
    this.serverCapabilities = {
      textDocumentSync: TextDocumentSyncKind.Incremental,
      hoverProvider: true,
      completionProvider: {
        resolveProvider: true,
        triggerCharacters: ['{', ' ', ';']
      } as CompletionOptions,
      signatureHelpProvider: {
        triggerCharacters: [' ']
      } as SignatureHelpOptions,
      definitionProvider: true,
      referencesProvider: true,
      documentHighlightProvider: true,
      renameProvider: {
        prepareProvider: true
      } as RenameOptions
    } as ServerCapabilities;

    // Create a connection for the server, using Node's IPC as a transport.
    // Also include all preview / proposed LSP features.

    const messageReader = new BrowserMessageReader(self);
    const messageWriter = new BrowserMessageWriter(self);
    
    this.Connection = createConnection(messageReader, messageWriter);

    // Create a simple text document manager.
    this.Documents = new TextDocuments(TextDocument);

    // The global settings, used when the `workspace/configuration` request is not supported by the client.
    // Please note that this is not the case when using this server with the client provided in this example
    // but could happen with other clients.
    this.defaultSettings = { maxNumberOfProblems: 1000 } as MetafSettings;
    this.globalSettings = this.defaultSettings;

    // Cache the settings of all open documents
    this.documentSettings = new Map();
    this.modelParsers = new Map();

    this.MetafValidator = new MetafValidator(this);
    this.MetafHoverProvider = new MetafHoverProvider(this);
    this.MetafCompletionProvider = new MetafCompletionProvider(this);
    this.MetafSignatureProvider = new MetafSignatureProvider(this);
    this.MetafDefinitionProvider = new MetafDefinitionProvider(this);
    this.MetafReferenceProvider = new MetafReferenceProvider(this);
    this.MetafHighlightProvider = new MetafHighlightProvider(this);
    this.MetafRenameProvider = new MetafRenameProvider(this);

    this.Connection.onInitialize(this.handleConnectionInitialize);
    this.Connection.onInitialized(this.handleConnectionInitialized);
    this.Connection.onDidChangeConfiguration(this.handleDidChangeConfiguration);
    this.Connection.onHover(this.handleHover);
    this.Connection.onCompletion(this.handleCompletion);
    this.Connection.onCompletionResolve(this.handleCompletionResolve);
    this.Connection.onSignatureHelp(this.handleSignatureHelp)
    this.Connection.onDefinition(this.handleDefinition)
    this.Connection.onReferences(this.handleReferences)
    this.Connection.onDocumentHighlight(this.handleDocumentHighlight)
    this.Connection.onPrepareRename(this.handlePrepareRename);
    this.Connection.onRenameRequest(this.handleRenameRequest);

    // Only keep settings for open documents
    this.Documents.onDidClose(e => {
      this.documentSettings.delete(e.document.uri);
      this.modelParsers.delete(e.document.uri);
    });

    // The content of a text document has changed. This event is emitted
    // when the text document first opened or when its content has changed.
    this.Documents.onDidChangeContent(change => {
      this.updateDocumentParsers(change.document.uri);
      this.validateTextDocument(change.document);
    });
  }

  public start() {
    // Make the text document manager listen on the connection
    // for open, change and close text document events
    this.Documents.listen(this.Connection);
    
    // Listen on the connection
    this.Connection.listen();
  }

  public getModelParser(documentUri: string) : ModelParser | undefined {
    let modelParser = this.modelParsers.get(documentUri);
    if (!modelParser) {
      this.updateDocumentParsers(documentUri);
    }

    return modelParser;
  }

  private updateDocumentParsers(documentUri: string) {
    const text = this.Documents.get(documentUri)?.getText() ?? "";
    const modelParser = new ModelParser(documentUri, text);
    modelParser.tryParse();
    this.modelParsers.set(documentUri, modelParser);
  }

  public validateTextDocument = async (textDocument: TextDocument): Promise<void> => {
    try {
      await this.MetafValidator.validateTextDocument(textDocument);
    } catch (e) { console.error(e) }
  }

  getDocumentSettings = (resource: string): Thenable<MetafSettings> => {
    if (!this.hasConfigurationCapability) {
      return Promise.resolve(this.globalSettings);
    }
    let result = this.documentSettings.get(resource);
    if (!result) {
      result = this.Connection.workspace.getConfiguration({
        scopeUri: resource,
        section: 'languageServerExample'
      });
      this.documentSettings.set(resource, result);
    }
    return result;
  }

  handleConnectionInitialize = (params: InitializeParams) => {
    this.ClientCapabilities = params.capabilities;
  
    const result: InitializeResult = {
      capabilities: this.serverCapabilities
    };

    if (this.hasWorkspaceFolderCapability) {
      result.capabilities.workspace = {
        workspaceFolders: {
          supported: true
        }
      };
    }

    return result;
  }

  handleConnectionInitialized = () => {
    if (this.hasConfigurationCapability) {
      // Register for all configuration changes.
      this.Connection.client.register(DidChangeConfigurationNotification.type, undefined);
    }
    if (this.hasWorkspaceFolderCapability) {
      this.Connection.workspace.onDidChangeWorkspaceFolders(_event => {
        this.Connection.console.log('Workspace folder change event received.');
      });
    }
  }

  handleDidChangeConfiguration = (change: DidChangeConfigurationParams) => {
    if (this.hasConfigurationCapability) {
      // Reset all cached document settings
      this.documentSettings.clear();
    }
    else {
      this.globalSettings = <MetafSettings>(change.settings.MetafLanguageServer || this.defaultSettings);
    }
  
    // Revalidate all open text documents
    this.Documents.all().forEach(this.validateTextDocument);
  }

  handleHover = (handleHover: HoverParams) : Hover | undefined => {
    try {
      return this.MetafHoverProvider.handleHover(handleHover);
    }
    catch (e) {
      console.log(`handleHover Exception: ${e}`);
    }
    return;
  }

  handleCompletion = (handleCompletion: CompletionParams): CompletionList | undefined => {
    try {
      return this.MetafCompletionProvider.handleCompletion(handleCompletion);
    }
    catch (e) {
      console.log(`handleCompletion Exception: ${e}`);
    }
  }

  handleCompletionResolve = (handleCompletionResolve: CompletionItem): CompletionItem => {
    try {
      return this.MetafCompletionProvider.handleCompletionResolve(handleCompletionResolve);
    }
    catch (e) {
      console.log(`handleCompletionResolve Exception: ${e}`);
    }

    return handleCompletionResolve;
  }

  handleSignatureHelp = (handleSignatureHelp: SignatureHelpParams) => {
    try {
      return this.MetafSignatureProvider.handleCompletion(handleSignatureHelp);
    }
    catch (e) {
      console.log(`handleSignatureHelp Exception: ${e}`);
    }
  }

  handleDefinition = (params: DefinitionParams): LocationLink[] => {
    try {
      return this.MetafDefinitionProvider.handleDefinition(params);
    }
    catch (e) {
      console.log(`handleDefintiion Exception: ${e}`);
    }
    return [];
  }

  handleReferences = (params: ReferenceParams): Location[] => {
    try {
      return this.MetafReferenceProvider.handleReferences(params);
    }
    catch (e) {
      console.log(`handleDefintiion Exception: ${e}`);
    }
    return [];
  }

  handleDocumentHighlight = (params: DocumentHighlightParams): DocumentHighlight[] => {
    try {
      return this.MetafHighlightProvider.handleDocumentHighlight(params);
    }
    catch (e) {
      console.log(`handleDefintiion Exception: ${e}`);
    }
    return [];
  }

  handlePrepareRename = (params: PrepareRenameParams): PrepareRenameResult | undefined => {
    try {
     return this.MetafRenameProvider.handlePrepareRename(params);
    }
    catch (e) {
      console.log(`handleDefintiion Exception: ${e}`);
    }
    return
  }

  handleRenameRequest = (params: RenameParams) : WorkspaceEdit | undefined => {
    try {
      return this.MetafRenameProvider.handleRename(params);
    }
    catch (e) {
      console.log(`handleDefintiion Exception: ${e}`);
    }
    return;
  }
}