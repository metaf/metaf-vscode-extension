import { PrepareRenameParams, PrepareRenameResult, Range, ReferenceClientCapabilities, ReferenceContext, ReferenceParams, RenameParams, TextEdit, WorkspaceEdit } from "vscode-languageserver";
import MetafLangServer from "./MetafLangServer";
import { ModelParser, MetaFParser, MetaFLexer, parser, BlockArgument, MetafModelBuilderVisitor, ActionBlock, ConditionBlock, WaypointBlock, MetafCompletionType, ParserModelBase, StateBlock } from "metaf-parser-ts";
import MetafHelpers from "./AntlrHelpers";
import * as c3 from "antlr4-c3"
import { ParserRuleContext } from "antlr4ng";

export default class MetafRenameProvider {
  langServer: MetafLangServer;

  constructor(langServer: MetafLangServer) {
    this.langServer = langServer; 
  }

  handleRename = (params: RenameParams): WorkspaceEdit | undefined => {
    const contexts = MetafHelpers.getAllSymbolsMatchingCurrentArg(this.langServer, params);

    return {
      changes: {
        [params.textDocument.uri]: contexts.map(ctx => {
          return {
            range: MetafHelpers.contextToRange(ctx),
            newText: (ctx.getPayload() as any)._value?.type == MetaFLexer.CURLYCONTENTS ? `{${params.newName}}` : params.newName
          } as TextEdit;
        })
      }
    } as WorkspaceEdit;
  }

  handlePrepareRename = (params: PrepareRenameParams): PrepareRenameResult | undefined => {
    const argInfo = MetafHelpers.getCurrentArg(this.langServer, params);
    if (argInfo) {
      switch (argInfo.Arg.CompletionType) {
        case MetafCompletionType.StateDef:
        case MetafCompletionType.StateRef: {
          const block = argInfo.Parser.meta?.States.find(s => s.Id == (argInfo.Model as any)[argInfo.Arg.Name]);
          if (block && block.Context?.start) {
            const range = MetafHelpers.contextToRange(argInfo.Arg.Context)
            if (range && argInfo.Arg.Context) {
              return {
                range: range,
                placeholder: argInfo.Arg.Context.getText()
              }
            }
          }
          break;
        }
        case MetafCompletionType.NavDef:
        case MetafCompletionType.NavRef: {
          const block = argInfo.Parser.meta?.NavRoutes.find(s => s.Id == (argInfo.Model as any)[argInfo.Arg.Name]);
          if (block && block.Context?.start) {
            const range = MetafHelpers.contextToRange(argInfo.Arg.Context)
            if (range && argInfo.Arg.Context) {
              return {
                range: range,
                placeholder: argInfo.Arg.Context.getText()
              }
            }
          }
          break;
        }
      }
    }
    return;
  }
}