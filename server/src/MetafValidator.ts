import { TextDocument } from "vscode-languageserver-textdocument";
import MetafLangServer from "./MetafLangServer";
import { MetafErrorType, MetafError } from "metaf-parser-ts";

import {
	Diagnostic,
	DiagnosticSeverity,
	DiagnosticTag,
	Position,
} from 'vscode-languageserver/browser';

export default class MetafValidator {
  private langServer: MetafLangServer;

  constructor(langServer: MetafLangServer) {
    this.langServer = langServer;
  }

  public async validateTextDocument(textDocument: TextDocument) {
    const settings = await this.langServer.getDocumentSettings(textDocument.uri);
  
    const parser = this.langServer.getModelParser(textDocument.uri);
    if (!parser) {
      return;
    }

    const diagnostics: Diagnostic[] = [];

    const errors: MetafError[] = [];
    errors.push(...parser.ErrorHandler.Errors);
    errors.push(...parser.ErrorStrategy.Errors);

    
    for (let i = 0; i < errors.length && i < settings.maxNumberOfProblems; i++) {
      diagnostics.push(this.createDiagnostic(errors[i]));
    }

    this.langServer.Connection.sendDiagnostics({ uri: textDocument.uri, diagnostics });
  }

  createDiagnostic(error: MetafError): Diagnostic {
    const startPos = Position.create(Math.max(0, error.Start.Line - 1), Math.max(0, error.Start.Column));
    const endPos = error.End ? Position.create(Math.max(0, error.End.Line - 1), Math.max(0, error.End.Column)) : startPos;
    
    const diagnostic: Diagnostic = {
      severity: this.metafErrorToSeverity(error),
      range: {
        start: startPos,
        end: endPos
      },
      message: error.Message,
      source: 'metaf-linter',
      tags: error.Type == MetafErrorType.Deprecated ? [ DiagnosticTag.Deprecated ] : []
    };
      
    /*
    if (this.langServer.hasDiagnosticRelatedInformationCapability) {
      diagnostic.relatedInformation = [
        {
          location: {
            uri: textDocument.uri,
            range: Object.assign({}, diagnostic.range)
          },
          message: error.RuleStack.map(idx => MetaFParser.ruleNames[idx]).join('->'),
        },
        {
          location: {
            uri: textDocument.uri,
            range: Object.assign({}, diagnostic.range)
          },
          message: `name:${error.Exception?.name} ctx:${MetaFParser.ruleNames[error.Exception?.context?.ruleIndex ?? 0]} ${error.Exception?.stack}`
        }
      ];
    }
    */

    return diagnostic;
  }

  metafErrorToSeverity(error: MetafError): DiagnosticSeverity | undefined {
    switch(error.Type) {
      case MetafErrorType.Deprecated:
      case MetafErrorType.Warning:
        return DiagnosticSeverity.Warning;
      default:
        return DiagnosticSeverity.Error;
    }
  }
}