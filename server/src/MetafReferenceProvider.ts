import { CompletionItem, CompletionList, Definition, DefinitionLink, DefinitionParams, DocumentUri, Location, Position, Range, ReferenceClientCapabilities, ReferenceContext, ReferenceParams } from "vscode-languageserver";
import MetafLangServer from "./MetafLangServer";
import { ModelParser, MetaFParser, MetaFLexer, parser, BlockArgument, MetafModelBuilderVisitor, ActionBlock, ConditionBlock, WaypointBlock, MetafCompletionType, ParserModelBase, StateBlock } from "metaf-parser-ts";
import MetafHelpers from "./AntlrHelpers";
import * as c3 from "antlr4-c3"
import { ParserRuleContext } from "antlr4ng";

export default class MetafReferenceProvider {
  langServer: MetafLangServer;

  constructor(langServer: MetafLangServer) {
    this.langServer = langServer; 
  }

  handleReferences = (params: ReferenceParams): Location[] => {
    const contexts = MetafHelpers.getAllSymbolsMatchingCurrentArg(this.langServer, params)

    return contexts.map(ctx => {
      const range = MetafHelpers.contextToRange(ctx);
      return range ? Location.create(params.textDocument.uri, range) : undefined;
    }).filter(loc => typeof(loc) != 'undefined') as Location[];
  }
}