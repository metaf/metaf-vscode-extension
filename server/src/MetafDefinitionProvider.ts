import { CompletionItem, CompletionList, Definition, DefinitionLink, DefinitionParams, DocumentUri, LocationLink, Position, Range } from "vscode-languageserver";
import MetafLangServer from "./MetafLangServer";
import { ModelParser, MetaFParser, MetaFLexer, parser, BlockArgument, MetafModelBuilderVisitor, ActionBlock, ConditionBlock, WaypointBlock, MetafCompletionType, ParserModelBase } from "metaf-parser-ts";
import MetafHelpers from "./AntlrHelpers";
import * as c3 from "antlr4-c3"

export default class MetafDefinitionProvider {
  langServer: MetafLangServer;

  constructor(langServer: MetafLangServer) {
    this.langServer = langServer; 
  }

  handleDefinition = (params: DefinitionParams): LocationLink[] => {
    const argInfo = MetafHelpers.getCurrentArg(this.langServer, params);
    if (argInfo) {
      let block: ParserModelBase | undefined;
      switch (argInfo.Arg.CompletionType) {
        case MetafCompletionType.StateRef: {
          block = argInfo.Parser.meta?.States.find(s => s.Id == (argInfo.Model as any)[argInfo.Arg.Name]);
          break;
        }
        case MetafCompletionType.NavRef: {
          block = argInfo.Parser.meta?.NavRoutes.find(s => s.Id == (argInfo.Model as any)[argInfo.Arg.Name]);
          break;
        }
      }

      if (block) {
        const range =  MetafHelpers.contextToRange(block.Context);
        if (range) {
          return [ LocationLink.create(params.textDocument.uri, range, range) ];
        }
      }
    }
    return [];
  }
}