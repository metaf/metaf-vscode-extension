import { CompletionItem, CompletionItemKind, CompletionItemLabelDetails, CompletionList, CompletionParams, InsertTextMode, MarkupKind, Position, Range, TextEdit } from "vscode-languageserver";
import MetafLangServer from "./MetafLangServer";
import { Token } from "antlr4ng";
import * as c3 from "antlr4-c3"
import MetafHelpers from "./AntlrHelpers";
import { BlockType, MetaFParser, MetaFLexer, ModelParser, models, MetafModelBuilderVisitor, parser, BlockArgument, MetafCompletionType, ConditionBlock, ActionBlock, WaypointBlock, ObjectClass, BlockInfo, NavRouteType } from "metaf-parser-ts";
import AllItems from "./data/AllItems";
export default class MetafCompletionProvider {
  langServer: MetafLangServer;

  constructor(langServer: MetafLangServer) {
    this.langServer = langServer;
  }

  handleCompletion = (params: CompletionParams): CompletionList => {
    //console.log(`handleCompletion: start: ${params.textDocument.uri}:`);
    const modelParser = this.langServer.getModelParser(params.textDocument.uri) as ModelParser;
    const currentToken = MetafHelpers.getCurrentToken(modelParser, params.position.line, params.position.character - 1);
    const document = this.langServer.Documents.get(params.textDocument.uri);

    if (!modelParser) {
      console.log(`handleCompletion: no model parser...`);
      return { items: [] as CompletionItem[] } as CompletionList;
    }

    if (!currentToken) {
      console.log(`handleCompletion: no current token...`);
      return { items: [] as CompletionItem[] } as CompletionList;
    }

    if (!document) {
      console.log(`handleCompletion: no document...`);
      return { items: [] as CompletionItem[] } as CompletionList;
    }

    const core = new c3.CodeCompletionCore(modelParser.Visitor.Parser);
    core.preferredRules = new Set([
      MetaFParser.RULE_conditionBlock,
      MetaFParser.RULE_actionBlock,
      MetaFParser.RULE_waypointBlock,
      MetaFParser.RULE_navBlock
    ]);
    core.ignoredTokens = new Set([
      MetaFLexer.NL,
      MetaFLexer.WS,
      MetaFLexer.EOF,
      MetaFLexer.INDENT,
      MetaFLexer.DEDENT,
      MetaFLexer.NUMBER,
      MetaFLexer.DECIMAL,
      MetaFLexer.HEX,
      MetaFLexer.IDENTIFIER,
      MetaFLexer.CURLYCONTENTS
    ]);
    const offset = document.offsetAt(params.position);

    const candidates = core.collectCandidates(currentToken.tokenIndex);
    let keywords: CompletionItem[] = [];
    for (let candidate of candidates.tokens) {
      if (!core.ignoredTokens.has(candidate[0])) {
        let label = modelParser.Visitor.Parser.vocabulary.getDisplayName(candidate[0]);
        if (!label) {
          continue;
        }
        label = label.replace(/^\'/igm,'').replace(/\'$/igm,'').trim();

        if (MetafModelBuilderVisitor.ColonBlockTokens.includes(candidate[0])) {
          label = label + ':';
        }

        keywords.push({
          label: label,
          kind: CompletionItemKind.Keyword
        } as CompletionItem);
      }
    }

   //console.log(`Handle completion: [${params.position.line}:${params.position.character}@${offset}] ${modelParser.Visitor.Lexer.vocabulary.getSymbolicName(currentToken.type)} tokens:${candidates.tokens.size} rules:${candidates.rules.size}`);

    let symbol = currentToken; // Find the symbol that covers your caret position.
    let functionNames: CompletionItem[] = [];
    let variableNames: CompletionItem[] = [];

    const modelInfo = modelParser?.Visitor.getTokenModelInfo(currentToken);
    if (!modelInfo || !modelInfo.Context) {
      return { items: [] as CompletionItem[] } as CompletionList;
    }

    const hasCurlyEnd = document.getText().length > offset ? document.getText()[offset] == '}' : false;

    const allConditions = Object.values(MetafModelBuilderVisitor.blockLookup).filter(c => c.Instance instanceof ConditionBlock);
    const allActions = Object.values(MetafModelBuilderVisitor.blockLookup).filter(c => c.Instance instanceof ActionBlock);
    const allWaypoints = Object.values(MetafModelBuilderVisitor.blockLookup).filter(c => c.Instance instanceof WaypointBlock);

    const addAll = (items: BlockInfo[]) => {
      for (const blockInfo of items) {
        functionNames.push({
          label: blockInfo.Instance.IdentifierName,
          kind: CompletionItemKind.Method,
          documentation: { kind: MarkupKind.Markdown, value: blockInfo.Instance.getMarkdownDocs() },
          labelDetails: {
            description: blockInfo.Instance.Summary
          }
        } as CompletionItem);
      }
    }

    for (let candidate of candidates.rules) {
      switch (candidate[0]) {
        case MetaFParser.RULE_actionBlock: {
          const ctx = (modelInfo.Model.Context as parser.ActionBlockContext);
          //console.log(`Condition: ${(ctx as any)['name']} ${!!ctx.block_id} // ${modelInfo.Model.IdentifierName} // ${!!(ctx.INDENT && ctx.INDENT())}`);
          // we have a valid block / parent block definition
          if (ctx.block_id && ctx.block_id()) {
            // typing an argument
            if (!(ctx.INDENT && ctx.INDENT() && ctx.argsList)) {
              const argIndex = MetafHelpers.getArgIndex(modelParser, ctx.argsList()?.arg().length || 0, currentToken, allActions.map(c => c.Instance.IdentifierName));
              keywords = keywords.concat(getArgCompletions(modelParser, argIndex, modelInfo.Model, hasCurlyEnd));
            }
            // typing a child block name
            else {
              addAll(allActions);
            }
          }
          // this is the state when typing the block identifier
          else {
            addAll(allActions);
          }
          break;
        }
    
        case MetaFParser.RULE_conditionBlock: {
          const ctx = (modelInfo.Model.Context as parser.ConditionBlockContext);
          //console.log(`Condition: ${(ctx as any)['name']} ${!!ctx.block_id} // ${modelInfo.Model.IdentifierName} // ${!!(ctx.INDENT && ctx.INDENT())}`);
          // we have a valid block / parent block definition
          if (ctx.block_id && ctx.block_id()) {
            // typing an argument
            if (!(ctx.INDENT && ctx.INDENT() && ctx.argsList)) {
              const argIndex = MetafHelpers.getArgIndex(modelParser, ctx.argsList()?.arg().length || 0, currentToken, allConditions.map(c => c.Instance.IdentifierName));
              keywords = keywords.concat(getArgCompletions(modelParser, argIndex, modelInfo.Model, hasCurlyEnd));
            }
            // typing a child block name
            else {
              addAll(allConditions);
            }
          }
          // this is the state when typing the block identifier
          else {
            addAll(allConditions);
          }
          break;
        }
    
        case MetaFParser.RULE_waypointBlock: {
          const ctx = (modelInfo.Model.Context as parser.WaypointBlockContext);
          // we have a valid block / parent block definition
          if (ctx.block_id && ctx.block_id() && ctx.argsList) {
            const argIndex = MetafHelpers.getArgIndex(modelParser, ctx.argsList()?.arg().length || 0, currentToken, allWaypoints.map(c => c.Instance.IdentifierName));
            keywords = keywords.concat(getArgCompletions(modelParser, argIndex, modelInfo.Model, hasCurlyEnd));
          }
          // this is the state when typing the block identifier
          else {
            addAll(allWaypoints);
          }
          break;
        }
     
        case MetaFParser.RULE_navBlock: {
          const ctx = (modelInfo.Model.Context as parser.NavBlockContext);
          if (ctx.argsList) {
            const argIndex = MetafHelpers.getArgIndex(modelParser, ctx.argsList()?.arg().length || 0, currentToken, allWaypoints.map(c => c.Instance.IdentifierName));
            keywords = keywords.concat(getArgCompletions(modelParser, argIndex, modelInfo.Model, hasCurlyEnd));
          }
          break;
        }
      }
    } 
    
    // Finally combine all found lists into one for the UI.
    // We do that in separate steps so that you can apply some ordering to each of your sub lists.
    // Then you also can order symbols groups as a whole depending their importance.
    let results: CompletionItem[] = [];
    results.push(...keywords);
    results.push(...functionNames);
    results.push(...variableNames);

    
    //console.log(`handleCompletion: finish: ${params.textDocument.uri}: found ${results.length} items. CurlyEnd: ${hasCurlyEnd}`);
    return {
      items: results as CompletionItem[]
    } as CompletionList
  }

  handleCompletionResolve = (item: CompletionItem): CompletionItem => {
    //console.log(`Handle completion resolve: ${item}`);
    return item;
  }
}

function toCurlyCompletionItem(i: string, type: CompletionItemKind, hasCurlyEnd: boolean) : CompletionItem {
  return {
    label: i,
    insertText: `{${i}${hasCurlyEnd ? '' : '}'}`,
    filterText: `{${i}}`,
    kind: type, 
  } as CompletionItem
}


function getArgCompletions(modelParser: ModelParser, argIndex: number, model: any, hasCurlyEnd: boolean): CompletionItem[] {
  let keywords = [] as CompletionItem[];

  if (argIndex < model.Arguments.length) {
    const arg = model.Arguments[argIndex] as BlockArgument;
    switch(arg?.CompletionType) {
      case MetafCompletionType.ItemName: {
        keywords = keywords.concat(AllItems.map(i => toCurlyCompletionItem(i, CompletionItemKind.Value, hasCurlyEnd)));
        break;
      }
      case MetafCompletionType.NavRef: {
        keywords = keywords.concat(modelParser.meta?.NavRoutes.map(i => {
          return {
            label: i.Id,
            kind: CompletionItemKind.Variable, 
          } as CompletionItem;
        }));
        break;
      }
      case MetafCompletionType.StateRef: {
        keywords = keywords.concat(modelParser.meta?.States.map(i => toCurlyCompletionItem(i.Id, CompletionItemKind.Variable, hasCurlyEnd)));
        break;
      }
      case MetafCompletionType.ObjectClass: {
        keywords = keywords.concat(Object.keys(ObjectClass).filter((v) => !isNaN(Number(v))).map(i => {
          return {
            label: `${i}: ${ObjectClass[i as any as number]}`,
            insertText: `${i}`,
            kind: CompletionItemKind.Enum, 
          } as CompletionItem;
        }));
        break;
      }
      case MetafCompletionType.NavRouteType: {
        keywords = keywords.concat(Object.keys(NavRouteType).filter((v) => isNaN(Number(v))).map(i => {
          return {
            label: i,
            kind: CompletionItemKind.Enum, 
          } as CompletionItem;
        }));
        break;
      }
      case MetafCompletionType.RecallSpellName: {
        keywords = keywords.concat(models.waypoints.RecallSpellWaypoint.ValidRecallSpellNames.map(i => toCurlyCompletionItem(i, CompletionItemKind.Variable, hasCurlyEnd)));
        break;
      }
      case MetafCompletionType.ChatColor: {
        break;
      }
    }
  }
  return keywords;
} 