import MetafLangServer from "./MetafLangServer";
import MetafHelpers from "./AntlrHelpers";
import { HoverParams, Hover, Position, MarkupContent, MarkupKind } from "vscode-languageserver";
import { BlockArgument, BlockType, MetaFParser, ModelParser, models} from "metaf-parser-ts";
import { ParserRuleContext } from "antlr4ng";

export default class MetafHoverProvider {
  langServer: MetafLangServer;

  constructor(langServer: MetafLangServer) {
    this.langServer = langServer; 
  }

  handleHover = (params: HoverParams): Hover | undefined => {
    const modelParser = this.langServer.getModelParser(params.textDocument.uri);
    const currentToken = MetafHelpers.getCurrentToken(modelParser, params.position.line, params.position.character);
    const document = this.langServer.Documents.get(params.textDocument.uri);

    if (currentToken && currentToken.type == MetaFParser.NOT_KEYWORD) {
      const fakeNot = new models.conditions.NotCondition(undefined as any, undefined);
      return {
        contents: [
          fakeNot.Description
        ]
      };
    }

    const modelInfo = modelParser?.Visitor.getTokenModelInfo(currentToken);
    if (document && currentToken && modelInfo && modelInfo.Context) {
      const ctx = modelInfo.Context as ParserRuleContext;
      switch(modelInfo.BlockType) {
        case BlockType.Argument:
          const arg = modelInfo.Model.Arguments[modelInfo.ArgumentIndex || 0] as BlockArgument;
          const tokenLen = currentToken.type == MetaFParser.CURLYCONTENTS ? (currentToken.text?.length || 0) + 2 : (currentToken.text?.length || 0);
          return {
            contents: [
              `### \`Argument #${(modelInfo.ArgumentIndex || 0) + 1}\`: **${arg.Name}**: ${arg.Description}`,
              modelInfo.Model.getMarkdownDocs()
            ],
            range: ctx && ctx.start?.start ? {
              start: document.positionAt(ctx.start.start),
              end: document.positionAt(ctx.start.start + tokenLen)
            } : undefined
          } as Hover;
        default:
          return {
            contents: [
              modelInfo.Model.getMarkdownDocs(),
            ],
            range: ctx && ctx.start?.start && ctx.stop?.stop ? {
              start: document.positionAt(ctx.start.start),
              end: document.positionAt(ctx.stop.stop)
            } : undefined
          };
      }
    }
  }
}