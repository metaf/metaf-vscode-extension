import { CompletionItem, CompletionItemKind, CompletionItemLabelDetails, CompletionList, CompletionParams, MarkupContent, MarkupKind, ParameterInformation, Position, SignatureHelp, SignatureHelpParams, SignatureInformation } from "vscode-languageserver/node";
import MetafLangServer from "./MetafLangServer";
import { Token } from "antlr4ng";
import * as c3 from "antlr4-c3"
import MetafHelpers from "./AntlrHelpers";
import { BlockType, MetaFParser, MetaFLexer, ModelParser, models, MetafModelBuilderVisitor, parser, BlockArgument, MetafCompletionType, ActionBlock, ConditionBlock } from "metaf-parser-ts";
import AllItems from "./data/AllItems";
export default class MetafSignatureProvider { 
  langServer: MetafLangServer;

  constructor(langServer: MetafLangServer) {
    this.langServer = langServer;
  }

  handleCompletion = (params: SignatureHelpParams): SignatureHelp | undefined => {
    //console.log(`Start handleSignature`)
    const modelParser = this.langServer.getModelParser(params.textDocument.uri) as ModelParser;
    const currentToken = MetafHelpers.getCurrentToken(modelParser, params.position.line, params.position.character - 1);
    const document = this.langServer.Documents.get(params.textDocument.uri);
    const modelInfo = modelParser?.Visitor.getTokenModelInfo(currentToken);

    if (!modelInfo || !currentToken)
      return;

      const ctx = (modelInfo.Model.Context as parser.ConditionBlockContext);
      // we have a valid block / parent block definition
      let argIndex = -1;
      if (ctx.block_id && ctx.block_id()) {
        // typing an argument
        if (!(ctx.INDENT && ctx.INDENT())) {
          if ((ctx.argsList()?.arg().length || 0) > 0) {
            let x = currentToken;
            const allConditionIdentifiers = Object.values(MetafModelBuilderVisitor.blockLookup).filter(c => c.Instance instanceof ConditionBlock).map(c => c.Instance.IdentifierName);
            let loop = 100000;
            while (x.tokenIndex > 0 && loop-- > 0) {
              if (x.type == MetaFLexer.IDENTIFIER && allConditionIdentifiers.includes(x.text || '')) {
                break;
              }
              x = modelParser.Visitor.TokenStream.get(x.tokenIndex - 1);
              if (x.type != MetaFLexer.WS) {
                argIndex++;
              }
            }
          }
          argIndex = Math.max(argIndex, 0)
        }
        // typing a child block name
        else {
          argIndex = -1
        }
      }
      // this is the state when typing the block identifier
      else {
        argIndex = -1
      }

      //console.log(`Finish handleSignature: ${argIndex}`)
      if (argIndex >= 0) {
        return {
          signatures: [
          {
            label: modelInfo.Model.IdentifierName + ` ${modelInfo.Model.Arguments.map(a => (a.DefaultValue === undefined ? a.Name : `${a.Name}?`)).join(' ')}`,
            documentation: {
              value: modelInfo.Model.getMarkdownDocs(),
              kind: MarkupKind.Markdown
            },
            parameters: modelInfo.Model.Arguments.map(a => {
              return {
                label: a.Name,
                documentation: {
                  value: a.Description,
                  kind: MarkupKind.Markdown
                } as MarkupContent
              } as ParameterInformation
            }),
            activeParameter: argIndex
          } as SignatureInformation 
          ]
        } as SignatureHelp
      }
  }
}