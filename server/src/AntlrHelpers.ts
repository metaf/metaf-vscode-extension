import { ParserRuleContext, Token } from "antlr4ng";
import { ModelParser, models, MetaFLexer, BlockArgument, ParserModelBase, MetaFParser, MetafModelBuilderVisitor, parser, ConditionBlock, ActionBlock, WaypointBlock, BlockType, MetafCompletionType } from "metaf-parser-ts";
import { CompletionItem, DefinitionParams, Position, Range } from "vscode-languageserver";
import MetafLangServer from "./MetafLangServer";
import * as c3 from "antlr4-c3"

const helpers = {
  getCurrentToken(modelParser: ModelParser | undefined, line: number, char: number) : Token | undefined {
    if (!modelParser)
      return;

    let current: Token | undefined; 
    line = line + 1;
    let loop = 100000;
    for (let i = 0; i < (modelParser?.Visitor.TokenStream.size || 0); i++) {
      const next = modelParser?.Visitor.TokenStream.get(i);
      const currentIsOnOrBefore = current && (current.line < line || (current.line == line && current.column <= char));
      const nextIsOnOrAfter = next && (next.line > line || (next.line == line && next.column > char));
      if (currentIsOnOrBefore && nextIsOnOrAfter) {
        return current;
      }
      current = next;
      if (loop-- <= 0)
        return next;
    }
  },

  getArgIndex(modelParser: ModelParser, argCount: number, currentToken: Token, identifiers: string[]): number {
    let argIndex = -1;
    if (argCount > 0) {
      let x = currentToken;
      let loop = 100000;
      while (x.tokenIndex > 0 && loop-- >= 0) {
        if (x.type == MetaFLexer.IDENTIFIER && identifiers.includes(x.text || '')) {
          break;
        }
        x = modelParser.Visitor.TokenStream.get(x.tokenIndex - 1);
        if (x.type != MetaFLexer.WS) {
          argIndex++;
        }
      }
    }
  
    return Math.max(argIndex, 0)
  },

  getArg(model: ParserModelBase, argIndex: number): BlockArgument | undefined {
    let keywords = [] as CompletionItem[];

    if (argIndex < model.Arguments.length) {
      return model.Arguments[argIndex] as BlockArgument;
    }
  },

  tokenToPosition(token: Token | undefined) : Position | undefined {
    if (token) {
      return Position.create(token.line - 1, token.column)
    }
  },

  contextToRange(ctx: ParserRuleContext | undefined): Range | undefined {
    if (ctx && ctx.start && ctx.stop) {
      if ((ctx.getPayload() as any)._value) {
        const len = ctx.getText().length + ((ctx.getPayload() as any)._value?.type == MetaFLexer.CURLYCONTENTS ? 2 : 0)
        const start = Position.create(ctx.start.line - 1, ctx.start.column)
        const stop = Position.create(ctx.start.line - 1, ctx.start.column + len)
        return Range.create(start, stop);
      }
      const start = Position.create(ctx.start.line - 1, ctx.start.column)
      const stop = Position.create(ctx.stop.line - 1, ctx.stop.column)
      return Range.create(start, stop);
    }
  },

  getCurrentArg(langServer: MetafLangServer, params: DefinitionParams): ArgInfo | undefined {
    const modelParser = langServer.getModelParser(params.textDocument.uri) as ModelParser;
    const currentToken = helpers.getCurrentToken(modelParser, params.position.line, params.position.character - 1);
    const document = langServer.Documents.get(params.textDocument.uri);

    if (!modelParser || !currentToken || !document) {
      return;
    }

    const core = new c3.CodeCompletionCore(modelParser.Visitor.Parser);
    core.preferredRules = new Set([
      MetaFParser.RULE_conditionBlock,
      MetaFParser.RULE_actionBlock,
      MetaFParser.RULE_waypointBlock,
      MetaFParser.RULE_navBlock,
      MetaFParser.RULE_stateBlock
    ]);
    core.ignoredTokens = new Set([
      MetaFLexer.NL,
      MetaFLexer.WS,
      MetaFLexer.EOF,
      MetaFLexer.INDENT,
      MetaFLexer.DEDENT,
      MetaFLexer.NUMBER,
      MetaFLexer.DECIMAL,
      MetaFLexer.HEX,
      MetaFLexer.IDENTIFIER,
      MetaFLexer.CURLYCONTENTS
    ]);
    const offset = document.offsetAt(params.position);

    const candidates = core.collectCandidates(currentToken.tokenIndex);

    const modelInfo = modelParser?.Visitor.getTokenModelInfo(currentToken);
    if (!modelInfo || !modelInfo.Context) {
      return
    }

    const allConditions = Object.values(MetafModelBuilderVisitor.blockLookup).filter(c => c.Instance instanceof ConditionBlock);
    const allActions = Object.values(MetafModelBuilderVisitor.blockLookup).filter(c => c.Instance instanceof ActionBlock);
    const allWaypoints = Object.values(MetafModelBuilderVisitor.blockLookup).filter(c => c.Instance instanceof WaypointBlock);



    let arg: BlockArgument | undefined;
    let argIndex = -1;

    if (document && currentToken && modelInfo && modelInfo.Context) {
      const ctx = modelInfo.Context as ParserRuleContext;
      switch(modelInfo.BlockType) {
        case BlockType.Argument: {
          arg = modelInfo.Model.Arguments[modelInfo.ArgumentIndex || 0] as BlockArgument;
          argIndex = modelInfo.ArgumentIndex || 0;
          break;
        }
        default: {
          for (let candidate of candidates.rules) {
            switch (candidate[0]) {
              case MetaFParser.RULE_actionBlock: {
                const ctx = (modelInfo.Model.Context as parser.ActionBlockContext);
                //console.log(`Condition: ${(ctx as any)['name']} ${!!ctx.block_id} // ${modelInfo.Model.IdentifierName} // ${!!(ctx.INDENT && ctx.INDENT())}`);
                // we have a valid block / parent block definition
                if (ctx.block_id && ctx.block_id()) {
                  // typing an argument
                  if (!(ctx.INDENT && ctx.INDENT() && ctx.argsList)) {
                    argIndex = helpers.getArgIndex(modelParser, ctx.argsList()?.arg().length || 0, currentToken, allActions.map(c => c.Instance.IdentifierName));
                    arg = helpers.getArg(modelInfo.Model, argIndex);
                    break;
                  }
                }
              }
          
              case MetaFParser.RULE_conditionBlock: {
                const ctx = (modelInfo.Model.Context as parser.ConditionBlockContext);
                //console.log(`Condition: ${(ctx as any)['name']} ${!!ctx.block_id} // ${modelInfo.Model.IdentifierName} // ${!!(ctx.INDENT && ctx.INDENT())}`);
                // we have a valid block / parent block definition
                if (ctx.block_id && ctx.block_id()) {
                  // typing an argument
                  if (!(ctx.INDENT && ctx.INDENT() && ctx.argsList)) {
                    argIndex = helpers.getArgIndex(modelParser, ctx.argsList()?.arg().length || 0, currentToken, allConditions.map(c => c.Instance.IdentifierName));
                    arg = helpers.getArg(modelInfo.Model, argIndex);
                    break;
                  }
                }
              }
          
              case MetaFParser.RULE_waypointBlock: {
                const ctx = (modelInfo.Model.Context as parser.WaypointBlockContext);
                // we have a valid block / parent block definition
                if (ctx.block_id && ctx.block_id() && ctx.argsList) {
                  argIndex = helpers.getArgIndex(modelParser, ctx.argsList()?.arg().length || 0, currentToken, allWaypoints.map(c => c.Instance.IdentifierName));
                  arg = helpers.getArg(modelInfo.Model, argIndex);
                  break;
                }
              }
          
              case MetaFParser.RULE_stateBlock: {
                const ctx = (modelInfo.Model.Context as parser.StateBlockContext);
                // we have a valid block / parent block definition
                if (ctx.argsList) {
                  argIndex = helpers.getArgIndex(modelParser, ctx.argsList()?.arg().length || 0, currentToken, ['STATE:']);
                  arg = helpers.getArg(modelInfo.Model, argIndex);
                  break;
                }
              }
          
              case MetaFParser.RULE_navBlock: {
                const ctx = (modelInfo.Model.Context as parser.NavBlockContext);
                // we have a valid block / parent block definition
                if (ctx.argsList) {
                  argIndex = helpers.getArgIndex(modelParser, ctx.argsList()?.arg().length || 0, currentToken, ['NAV:']);
                  arg = helpers.getArg(modelInfo.Model, argIndex);
                  break;
                }
              }
            }
          }
        }
      }
    }

    if (arg) {
      return {
        Arg: arg,
        Model: modelInfo.Model,
        Parser: modelParser,
        ArgValue: argIndex > -1 ? (modelInfo.Model as any)[arg.Name] : undefined
      } as ArgInfo;
    }
  },

  getAllSymbolsMatchingCurrentArg(langServer: MetafLangServer, params: DefinitionParams): ParserRuleContext[] {
    const argInfo = helpers.getCurrentArg(langServer, params);
    let contexts: ParserRuleContext[] = [];
    if (argInfo) {

      switch (argInfo.Arg.CompletionType) {
        case MetafCompletionType.StateDef:
        case MetafCompletionType.StateRef: {
          for (const state of argInfo.Parser.meta?.States) {
            helpers.addRecursiveStateRefs(contexts, state, argInfo.ArgValue);
            for (const rule of state.Rules) {
              helpers.addRecursiveStateRefs(contexts, rule.Condition, argInfo.ArgValue);
              helpers.addRecursiveStateRefs(contexts, rule.Do.Action, argInfo.ArgValue);
            }
          }
          break;
        }
        case MetafCompletionType.NavDef:
        case MetafCompletionType.NavRef: {
          for (const navRoute of argInfo.Parser.meta?.NavRoutes) {
            helpers.addRecursiveNavRefs(contexts, navRoute, argInfo.ArgValue)
          }
          for (const state of argInfo.Parser.meta?.States) {
            for (const rule of state.Rules) {
              helpers.addRecursiveNavRefs(contexts, rule.Condition, argInfo.ArgValue);
              helpers.addRecursiveNavRefs(contexts, rule.Do.Action, argInfo.ArgValue);
            }
          }
          break;
        }
      }
    }

    return contexts;
  },
  
  addRecursiveStateRefs(contexts: ParserRuleContext[], block: ParserModelBase, stateName: string) {
    const args = block.Arguments.filter(a => a.CompletionType == MetafCompletionType.StateDef || a.CompletionType == MetafCompletionType.StateRef);
    for (const arg of args) {
      if ((arg.CompletionType == MetafCompletionType.StateDef || arg.CompletionType == MetafCompletionType.StateRef) && arg.Context && (block as any)[arg.Name] == stateName) {
        contexts.push(arg.Context);
      }
    }
    for (const child of block.Children) {
      this.addRecursiveStateRefs(contexts, child, stateName);
    }
  },

  addRecursiveNavRefs(contexts: ParserRuleContext[], block: ParserModelBase, navId: string) {
    const args = block.Arguments.filter(a => a.CompletionType == MetafCompletionType.NavDef || a.CompletionType == MetafCompletionType.NavRef);
    for (const arg of args) {
      if ((arg.CompletionType == MetafCompletionType.NavDef || arg.CompletionType == MetafCompletionType.NavRef) && arg.Context && (block as any)[arg.Name] == navId) {
        contexts.push(arg.Context);
      }
    }
    for (const child of block.Children) {
      this.addRecursiveNavRefs(contexts, child, navId);
    }
  }
}

export interface ArgInfo {
  Arg: BlockArgument,
  Model: ParserModelBase,
  Parser: ModelParser,
  ArgValue: any
}

export default helpers;