import { CompletionItem, CompletionList, Definition, DefinitionLink, DefinitionParams, DocumentHighlight, DocumentHighlightKind, DocumentHighlightParams, DocumentUri, Location, Position, Range, ReferenceClientCapabilities, ReferenceContext, ReferenceParams } from "vscode-languageserver";
import MetafLangServer from "./MetafLangServer.js";
import { ModelParser, MetaFParser, MetaFLexer, parser, BlockArgument, MetafModelBuilderVisitor, ActionBlock, ConditionBlock, WaypointBlock, MetafCompletionType, ParserModelBase, StateBlock } from "metaf-parser-ts";
import MetafHelpers from "./AntlrHelpers";
import * as c3 from "antlr4-c3"
import { ParserRuleContext } from "antlr4ng";

export default class MetafHighlightProvider {
  langServer: MetafLangServer;

  constructor(langServer: MetafLangServer) {
    this.langServer = langServer; 
  }

  handleDocumentHighlight = (params: DocumentHighlightParams): DocumentHighlight[] => {
    const contexts = MetafHelpers.getAllSymbolsMatchingCurrentArg(this.langServer, params)

    console.log(`handleDocumentHighlight: ${contexts.length}`)
    
    const mapped = contexts.map(ctx => {
      const range = MetafHelpers.contextToRange(ctx);
      console.log(`\t\t${ctx.getText()}: ${range?.start.character}:${range?.end.character}`)
      return range ? {
        range: range,
      } as DocumentHighlight : undefined;
    }).filter(loc => typeof(loc) != 'undefined') as DocumentHighlight[];

    return mapped;
  }
}