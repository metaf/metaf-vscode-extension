# Metaf Visual Studio Code Extension

Welcome to Metaf, a Visual Studio Code extension designed to enhance your experience when working with the [Metaf](https://github.com/JJEII/metaf) scripting language, widely used for VTank metas. Whether you're a seasoned developer or new to programming, this extension aims to provide a seamless environment for coding and scripting.

## Features

### 1. Syntax Highlighting
Metaf extension enriches your code with syntax highlighting, making it easier to distinguish different elements in your scripts.

### 2. Linting
Ensure clean and error-free Metaf scripts with instant feedback from the built-in linter.

### 3. Code Completion
Effortlessly write Metaf code with intelligent code completion, reducing errors and speeding up your development process.

### 4. Inline Documentation Tooltips
Learn about Metaf functions and syntax on the fly with helpful tooltips that appear as you type, providing instant guidance.

### 5. Symbol Renaming and Highlighting
Simplify code maintenance by renaming state / nav identifiers with ease. The extension also highlights all instances of a state / nav block, ensuring you don't miss any changes.

### 6. Find All Symbol References/Definitions
Navigate your codebase effortlessly by quickly finding all references or definitions of a state / nav block, improving code comprehension.

## Getting Started

1. Install the [Metaf](https://marketplace.visualstudio.com/items?itemName=metaf.metaf) extension from the Visual Studio Code Marketplace.
2. Open a file with a `.af` extension, and you're ready to go!

## Support and Contributions

If you encounter any issues or have suggestions for improvement, feel free to [report them on GitLab](https://gitlab.com/metaf/metaf-vscode-extension/-/issues). Contributions are welcome!

Happy coding with Metaf!