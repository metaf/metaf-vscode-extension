/* --------------------------------------------------------------------------------------------
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 * ------------------------------------------------------------------------------------------ */

import { ExtensionContext, Uri } from 'vscode';
import { LanguageClientOptions } from 'vscode-languageclient';

import { LanguageClient } from 'vscode-languageclient/browser';

let client: LanguageClient;

export function activate(context: ExtensionContext) {
	console.log(`Metaf Language Extension is enabled!`)

	const clientOptions: LanguageClientOptions = {
		documentSelector: [{ language: 'metaf' }]
	};

	const client = createWorkerLanguageClient(context, clientOptions);

	client.start();
	console.log(`Metaf Language Extension is enabled! 123`, client)
}

export function deactivate(): Thenable<void> | undefined {
	if (!client) {
		return undefined;
	}
	return client.stop();
}


function createWorkerLanguageClient(context: ExtensionContext, clientOptions: LanguageClientOptions) {
	// Create a worker. The worker main file implements the language server.
	const serverMain = Uri.joinPath(context.extensionUri, 'server/dist/browserServerMain.js');
	const worker = new Worker(serverMain.toString(true));

	// create the language server client to communicate with the server running in the worker
	return new LanguageClient('metaf-language-server', 'Metaf Language Server', clientOptions, worker);
}